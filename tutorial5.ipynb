{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Genitive alternations\n",
    "\n",
    "- Yves Scherrer, October 2019\n",
    "\n",
    "The previous tutorials focused mostly on change in content: positive vs. negative affect, semantic change, posts that convey hate speech, ...\n",
    "\n",
    "In this tutorial, we will look at **morphosyntactic change**, namely how different genitive constructions have changed over time in English. It is based on the 2013 paper by Wolk, Bresnan, Rosenbach and Szmrecsanyi [Dative and genitive variability in Late Modern English:\n",
    "Exploring cross-constructional variation and change](https://pdfs.semanticscholar.org/6f27/a968fc956dd2e27e14caefe57075e19bceba.pdf). The data for this study is extracted from the ARCHER corpus (*A Representative Corpus of Historical English Registers*).\n",
    "\n",
    "In English, there are two types of **genitive constructions**:\n",
    "- the ***s*-genitive**: *the Seneschal**'s** brother*\n",
    "- the ***of*-genitive**: *the brother **of** the Seneschal*\n",
    "\n",
    "The paper also studies dative alternations, but we will focus on genitives in this tutorial. We will examine how the frequencies of the two constructions change over time and which factors influence the choice of the construction."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Question:**\n",
    "- In present-day English, both genitive constructions occur and are grammatical. Do you have any idea what type of change might have occurred over time?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## A first look at the data\n",
    "\n",
    "The data is provided in the form of a CSV file. You can click on the links below to inspect the data file:\n",
    "- [genitives.csv](data/genitives.csv)\n",
    "\n",
    "Let's load the file into a dataframe:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pandas as pd\n",
    "import matplotlib.pyplot as plt\n",
    "%matplotlib inline\n",
    "import seaborn as sns"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "df = pd.read_csv('data/genitives.csv')\n",
    "\n",
    "print(df.head())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "Each row represents a single occurrence of a genitive. The most important fields are the following ones:\n",
    "- **realization**: whether the genitive is realized as an `of-genitive` or as a `s-genitive`\n",
    "- **year**: the year of the occurrence\n",
    "- **period**: the 50-year period if which the occurrence is located. The periods are defined as follows:\n",
    "  - 2: 1650-1699\n",
    "  - 3: 1700-1749\n",
    "  - 4: 1750-1799\n",
    "  - 5: 1800-1849\n",
    "  - 6: 1850-1899\n",
    "  - 7: 1900-1949\n",
    "  - 8: 1950-1999\n",
    "- Various linguistic features are defined in the columns **possessorPhrase, possessumPhrase, possessorHead, prunedPossessorHead, animacyPossessor, definitenessPossessor, finalSibilant, possessumLengthCharacters,  possessorLengthCharacters, clPossessumLength, clPossessorLength, prototypicality**. We will look at them in detail later.\n",
    "- Additional metadata are defined in the columns **filename, register, cent1800**."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's start by plotting the data by year. We'll use so-called \"violin plots\", in which higher frequencies are visualized by wider areas. We'll plot the year on the x-axis, and two \"violins\" for the two realization types:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "outputs": [],
   "source": [
    "sns.catplot(x=\"year\", y=\"realization\", kind='violin', data=df);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Questions:**\n",
    "- Can you observe a tendency of morphosyntactic change in this plot?\n",
    "- Does the plot support or contradict your initial hypothesis?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Aggregated data\n",
    "\n",
    "Let's create a line plot with the total counts of each period (analogously to Figure 1 in the paper). For this, we aggregate the counts in a cross-tabulation, compute per-row totals and reformat the dataframe again to make it plottable by seaborn:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# aggregate all datapoints of a particular period and realization, count frequencies,\n",
    "# and compute totals per row and column\n",
    "df_aggr = pd.crosstab(df['period'], df['realization'], margins=True, margins_name='total')\n",
    "# remove total row and put period in a separate column\n",
    "df_aggr = df_aggr.drop(index='total').reset_index()\n",
    "print(df_aggr)\n",
    "# reformat again to have one row for each count\n",
    "df_aggr2 = pd.melt(df_aggr, 'period', ['of-genitive', 's-genitive', 'total'], value_name='count')\n",
    "print(df_aggr2)\n",
    "\n",
    "sns.lineplot(x='period', y='count', hue='realization', data=df_aggr2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This figure shows that the total amount of genitives varies per period. We don't want the proportions between the two genitive types to be influenced by this variation in totals. Let's try again using relative frequencies instead:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# aggregate all datapoints of a particular period and realization, count frequencies, and normalize them by row\n",
    "df_aggr = pd.crosstab(df['period'], df['realization'], normalize='index').reset_index()\n",
    "print(df_aggr)\n",
    "# reformat again to have one row for each count\n",
    "df_aggr2 = pd.melt(df_aggr, 'period', ['of-genitive', 's-genitive'], value_name='frequency')\n",
    "print(df_aggr2)\n",
    "\n",
    "sns.lineplot(x='period', y='frequency', hue='realization', data=df_aggr2)\n",
    "# alternative: bar plot\n",
    "#sns.barplot(x='period', y='frequency', hue='realization', data=df_aggr2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Questions:**\n",
    "- How does the distribution of the two genitive realizations change over time?\n",
    "- Can you see some turning point in the evolution? If so, do you have any hypothesis what could have happened?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Statistical modeling\n",
    "\n",
    "### Linguistic factors of genitive change\n",
    "\n",
    "The existing literature states several linguistic factors that have been found to influence the choice of genitive realization:\n",
    "- **Syntactic weight:** One of the most well-known factors that influence the ordering of constituents in general is the principle of ‘end weight’, according to which ‘heavier’ (i.e. longer and/or more complex) constituents tend to follow ‘lighter’ ones (Wasow, 2002). Here, the syntactic weight is defined as the length of the constituent (possessor or possessum) in characters. The `clPossessumLength` and `clPossessorLength` fields contain values that result from a logarithmic transformation (to reduce skewness) and from centering around 50-year means. Thus, negative values represent lower-than-average syntactic weight, positive values represent higher-than-average weight.\n",
    "- **Animacy:** Previous studies have reported reliable and strong effects of animacy (cf. Rosenbach, 2002; Hinrichs and Szmrecsanyi, 2007). In this study, the possessor was annotated with one of five animacy categories (see field `animacyPossessor `):\n",
    "  - *animate:* humans, higher animals and sentient human-like beings such as gods\n",
    "  - *collective:* organizations (administration, church, ...), stable groups of humans (delegation, family, enemy, ...)\n",
    "  - *temporal:* points in time and durations\n",
    "  - *locatives:* locations\n",
    "  - *inanimate:* other concrete or non-concrete noun phrases.\n",
    "- **Definiteness:** The definiteness annotation provided in field `definitenessPossessor` comprises the three following values:\n",
    "  - *indefinite*\n",
    "  - *definite*\n",
    "  - *proper name:* This category includes prototypical proper nouns, as in *Wilhelm*, but also titles such as *the king of England* and names of institutions such as the *Medical Society*. Its value in the dataframe is `definite-propername`.\n",
    "- **Final sibilancy:** The literature suggests a clear, phonologically motivated preference for using the of-genitive with possessors ending in a sibilant (e.g. *-s, -sh, -ch*), an effect that is reliable across a multitude of corpora covering both spoken and written language (e.g., Grafmiller 2014; Szmrecsanyi and Hinrichs 2008). The field `finalSibilant` states whether the possessor ends with a sibilant.\n",
    "- **Semantic relation:** Genitives may encode a wide range of different semantic relations, which are notoriously difficult to classify. The data used in this study is coded as a binary distinction between prototypical and non-prototypical possessive relations (Rosenbach 2002; Koptjevskaja-Tamm 2002) in the field `prototypicality`:\n",
    "  - *prototypical relations:* legal ownership, body parts, kinship, part-whole relation\n",
    "  - *non-prototypical relations:* all other, e.g. *the law of the country*, *Mr. Wallack’s acting as the hero*, *the granting of such passports*, *the name of God*, ...\n",
    "\n",
    "Let us create a first statistical regression model to get an intuition about the importance of these features. We don't care about time and temporal change yet. We use the original dataframe `df`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "outputs": [],
   "source": [
    "import statsmodels.api as sm\n",
    "import statsmodels.formula.api as smf\n",
    "\n",
    "# create a generalized linear model of the binomial family (the realization is a binary variable)\n",
    "model = smf.glm(formula=\"realization ~ clPossessumLength + clPossessorLength + animacyPossessor + \\\n",
    "    definitenessPossessor + finalSibilant + prototypicality\", data=df, family=sm.families.Binomial())\n",
    "result = model.fit()\n",
    "print(result.summary2())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "The coefficients describe whether the predictor variables favor the *of-genitive* realization (positive values) or the *s-genitive* realization (negative values).\n",
    "\n",
    "For the categorical variables (`animacyPossessor`, `definitenessPossessor`, ...), one value is missing. The displayed coefficients denote differences with respect to the missing value. For example, the value `inanimate` is much more likely to favor *of-genitive* than the default (missing) value `animate`. \n",
    "\n",
    "**Questions:**\n",
    "- Discuss the obtained coefficients. Do they correspond to your expectations and/or to prior research as summarized above?\n",
    "- Are there any factors that turn out to be statistically insignificant? If so, which ones?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "source": [
    "### Random effects\n",
    "\n",
    "It is possible that some authors have personal (or idiolectal) preferences for one or the other realization, and that these personal preferences cannot be explained by the linguistic factors enumerated above.\n",
    "\n",
    "Personal preference is a typical example of a **random effect**: random effects capture variation dependent on open-ended, potentially hierarchical and unbalanced groups, such as the group of authors in the dataset. Traditional estimation of these idiosyncracies via **fixed effects** is not viable, but it would not be advisable to leave this information completely out\n",
    "of the model (as in our first attempt above). Furthermore, the issue whether the behavior of, say, a given author is statistically significantly different from another author – the question ultimately answered by fixed effect modeling – is not relevant for present purposes. Random effects, then, provide a sophisticated yet elegant method for taking such variation into account, making sure that the estimation of the interesting variables can proceed unaffected by this noise and that the results are easy to generalize.\n",
    "\n",
    "If we adopt the reasonable assumption that each corpus file has a different author, we can just use the `filename` column to model the authors' personal preferences.\n",
    "\n",
    "Another random effect are lexical effects: it is possible that some lemmas have an inherent preference for one or the other realization. In the dataset, the lemmas of the possessor head nouns are coded in the `prunedPossessorHead` column, replacing all lemmas with less than four observations by the generic value `OTHER`.\n",
    "\n",
    "Let's try to fit a statistical model with random effects. As the outcome variable is binary, the ideal model would be one based on binomials (as above). Unfortunately, the `statsmodels` package does not support the combination of binomials and random effects yet, so we take the next best solution: we convert the outcome variable into a numeric one and use a mixed linear regression model (`mixedlm`) instead."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "# the output variable needs to be numeric for this model type\n",
    "# create a new column in the dataframe that contains 1 if of-genitive and 0 if s-genitive\n",
    "df['realizationOf'] = (df['realization'] == 'of-genitive').astype(int)\n",
    "\n",
    "# the statsmodels implementation only allows one random effect\n",
    "# create a new column that contains all combinations of possessor heads and filenames\n",
    "df['lexicalXpersonal'] = df['prunedPossessorHead'] + '_' + df['filename']\n",
    "#print(df.head())\n",
    "\n",
    "# create the model, specifying the random effects with the groups parameter\n",
    "model = smf.mixedlm(\"realizationOf ~ clPossessumLength + clPossessorLength + animacyPossessor + \\\n",
    "    definitenessPossessor + finalSibilant + prototypicality\", data=df, groups='lexicalXpersonal')\n",
    "result = model.fit()\n",
    "print(result.summary())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "The values for the coefficients have changed, but the general tendency has remained the same.\n",
    "\n",
    "Let us look at the coefficients of the random variable and display the 10 word/text combinations that are most closely associated with *of-genitive* and with *s-genitive*:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# load the random effects into a dataframe\n",
    "random_effects = pd.DataFrame(result.random_effects).T\n",
    "# sort it with highest values on top\n",
    "random_effects = random_effects.sort_values('lexicalXpersonal', ascending=False)\n",
    "# print the 10 highest and 10 lowest values\n",
    "print('Features most associated with of-genitive:')\n",
    "print(random_effects.head(10))\n",
    "print()\n",
    "print('Features most associated with s-genitive:')\n",
    "print(random_effects.tail(10))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Questions:**\n",
    "- Can you find any tendencies?\n",
    "- Results might be clearer if only one of the two random effects were included in the model. Try recreating a model that includes `prunedPossessorHead` as the only random effect and describe the results. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Including time interactions\n",
    "\n",
    "We can assume that some of the linguistic factors become more or less important over time. Let's modify our statistical model by including an interaction with time.\n",
    "\n",
    "We could use the actual year numbers (as available in the column `year`), but we instead opt for a transformed version. To ease the assessment of diachronic changes and make statistical analyses more reliable, the individual dates were centered around 1800 and converted to centuries, so that a text from 1651 would count as -1.49 (149 years before 1800) and a text from 1931 as 1.31 (131 years after 1800). These transformed time stamps are given in the column `cent1800`.\n",
    "\n",
    "Let's see now how one of the linguistic features, `animacyPossessor`, interacts with time."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "model_time = smf.mixedlm(\"realizationOf ~ clPossessumLength + clPossessorLength + animacyPossessor * cent1800 + \\\n",
    "    definitenessPossessor + finalSibilant + prototypicality\", data=df, groups='lexicalXpersonal')\n",
    "result_time = model_time.fit()\n",
    "print(result_time.summary())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "These results can be interpreted as follows:\n",
    "- The non-interaction coefficients (lines 2-5) compare animate possessors to the other types for the year 1800, with all other types reliably more likely to occur with the *of-genitive* (as shown by the positive sign of the coefficient).\n",
    "- There is no reliable real-time change independent of animacy, as evidenced by the small, non-significant coefficient for `cent1800`.\n",
    "- There are significant interaction effects between real time and collective, locative, and temporal possessors. Notice that all interaction coefficients are negative, indicating that in real time, these three types become increasingly less likely to occur with the *of-genitive* (or in other words, more and more likely to occur with the *s-genitive*).\n",
    "\n",
    "To quantify the size of this change, the relevant coefficients are simply multiplied with their numeric values, summed and exponentiated (in the Python statistics module, coefficients are presented as logarithms and have to be transformed back to real values) For example, a collective possessor in 1800 is $\\exp(0.271 + (0.002 \\times 0) + (-0.050 \\times 0)) = 1.31$ times\n",
    "as likely to appear with the *of-genitive* than an animate possessor, while in 1950 it would only be $\\exp (0.271 + (0.002 \\times 1.5) + (-0.050 \\times 1.5)) = 1.22$ times as likely."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "**Questions:**\n",
    "\n",
    "- Change the formula of the model to include the time interaction with a different linguistic factor (one factor at a time, removing the one from `animacyPossessor`). Can you find other factors that influence the change?\n",
    "- Can you find factors that influence the change in the opposite direction, i.e. make *of-genitive* more likely over time?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Interrupted time series\n",
    "\n",
    "Until now, we have assumed that change over time is uniform, i.e. always goes in the same direction. However, judging from the V-shape in the initial plots, this probably is not true. The violin plot suggests that there is some turning point around 1820. Let us add another column to the dataset that indicates whether an observation occurs before or after this turning point.\n",
    "\n",
    "We can now add the interaction with this variable to the model. Note that this is a simpler version of the interrupted time series model used in the previous tutorial, as the year itself is not part of the model."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "df['after1820'] = (df['year'] >= 1820)\n",
    "model_its = smf.mixedlm(\"realizationOf ~ clPossessumLength * after1820 + clPossessorLength * after1820 \\\n",
    "    + animacyPossessor * after1820 + definitenessPossessor * after1820 + finalSibilant * after1820 \\\n",
    "    + prototypicality * after1820\", data=df, groups='lexicalXpersonal')\n",
    "result_its = model_its.fit()\n",
    "print(result_its.summary())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "**Questions:**\n",
    "\n",
    "- Which coefficents change most at the \"turning point\"?\n",
    "- Do the results agree with the ones obtained in the previous models?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## And finally some plots again...\n",
    "\n",
    "Let us look more closely at the `animacyPossessor` feature again. The results of the statistical models suggest diachronic change for the *locative* and *temporal* categories, but less so for the *collective* and *inanimate* categories.\n",
    "\n",
    "We will now create a line plot for the five animacy categories to visualize their evolution. Our goal is to display the percentage of *s-genitive* realizations per animacy category and per period. For this, we need to aggregate the data and normalize the rows by their totals:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# create one row per period/realization/animacyPossessor combination and compute its count\n",
    "df_counts = df[['period', 'realization', 'animacyPossessor', 'uniqueid']].groupby(['period', 'realization',\n",
    "                    'animacyPossessor']).count().rename(columns={'uniqueid': 'count'})\n",
    "# there are no inanimate s-genitives at period 8, so we just add the row explicitly with a count of 0\n",
    "df_counts.at[(8, 's-genitive', 'inanimate'), 'count'] = 0 \n",
    "\n",
    "# create one row per period/animacyPossessor combination (i.e. combining s and of) and compute its count\n",
    "df_totals = df[['period', 'animacyPossessor', 'uniqueid']].groupby(['period',\n",
    "                    'animacyPossessor']).count().rename(columns={'uniqueid': 'count'})\n",
    "\n",
    "# normalize the counts in df_count by the totals\n",
    "df_counts = (df_counts / df_totals).reset_index()\n",
    "\n",
    "# only keep the s-genitive values\n",
    "df_counts = df_counts[df_counts['realization']=='s-genitive']\n",
    "\n",
    "# make a line plot\n",
    "plot = sns.lineplot(x='period', y='count', hue='animacyPossessor', data=df_counts)\n",
    "plot.set(ylabel='proportion of s-genitive realizations');"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Question:**\n",
    "- Describe how the different animacy categories evolve over time. Which categories contribute most to the general shift towards s-genitives?\n",
    "- Above, we have assumed the existence of a \"turning point\" around 1820. Does this assumption have to be revised in the light of this plot?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Facultative additional exercise\n",
    "\n",
    "The paper cited above discusses not only genitive alternations, but also **dative alternations**.\n",
    "\n",
    "In English, dative alternations occur together with verbs like *to give* or *to write* that take two objects. The alternatives are the following:\n",
    "- the **ditransitive** dative construction: *I wrote Michael a note*\n",
    "- the **prepositional** dative construction: *I wrote a note **to** Michael*\n",
    "\n",
    "The data for the dative alternations is in the file `data/datives.csv`:\n",
    "- [datives.csv](data/datives.csv)\n",
    "\n",
    "Create a copy of this notebook, adapt the path to the file and work through it again. Note that the dative data will not have exactly the same format: the values in `realization` will change, and the set of columns will also be different."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
