{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Detecting semantic change with dynamic word embeddings\n",
    "\n",
    "- by Jacob Eisenstein, May 2019\n",
    "- Adapted by Yves Scherrer, September 2019\n",
    "\n",
    "Two weeks ago, we looked at a simple method to identify neologisms, i.e. new word forms. Now we'll explore how words change meaning over time, using dynamic word embeddings. The specific goals are:\n",
    "\n",
    "- Identify near-neighbors of words at various periods in time.\n",
    "- Align word embeddings over time by solving an orthogonal Procrustes problem.\n",
    "- Rank words by semantic change from 1900 to 1990, using global (embedding-based/\"first-order\") and local (neighbor-based/\"second-order\") techniques.\n",
    "\n",
    "Let's start by loading the required modules."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "outputs": [],
   "source": [
    "import pickle\n",
    "import numpy as np\n",
    "import pandas as pd\n",
    "import matplotlib.pyplot as plt\n",
    "%matplotlib inline\n",
    "import seaborn as sns"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Data loading\n",
    "\n",
    "Because training dynamic word embeddings is time-consuming, we'll use pretrained embeddings from [Hamilton et al](https://github.com/williamleif/histwords).\n",
    "\n",
    "The data is compressed in two files, one that lists the vocabulary items and one that contains the embedding matrices. The following code loads this into a dictionary of pandas dataframes. The data contains embeddings for the years 1800, 1850, 1900, 1950 and 1990."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "vocabularies = pickle.load(open('data/embeddings-over-time-vocabs.pkl', 'rb'))\n",
    "years = list(vocabularies.keys())\n",
    "print(years)\n",
    "\n",
    "vectors = {}\n",
    "vector_arrays = np.load('data/embeddings-over-time.npz')\n",
    "for vec, year in zip(vector_arrays.values(), years):\n",
    "    vectors[year] = pd.DataFrame(data=vec, index=vocabularies[year])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Let's inspect the data:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "# get the first lines of the 1800 embeddings and of the 1950 embeddings:\n",
    "print(vectors[1800].head())\n",
    "print()\n",
    "print(vectors[1950].head())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's compute the norm of each row in the 1900 dataset:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "norms = vectors[1900].apply(lambda x: np.sqrt(x.dot(x)), axis=1)\n",
    "print(norms.shape)\n",
    "print(norms.head())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "Vectors are normalized, they have a Euclidean norm of $1$. There are 10,000 words per year."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Near neighbors\n",
    "\n",
    "Let's start by computing some near neighbors. In general, this is done by computing cosine similarity between word vectors,\n",
    "\\begin{equation}\n",
    "s(\\vec{x},\\vec{y}) = \\frac{\\vec{x} \\cdot \\vec{y}}{||\\vec{x}|| \\times || \\vec{y} ||}\n",
    "\\end{equation}\n",
    "\n",
    "However, as we have seen, the embeddings have a guaranteed norm of 1. Thus, the denominator can be ignored and we need only consider the numerator (dot product).\n",
    "\n",
    "To compute the $K$ nearest neighbors of word $x$, we compute $s(\\vec{x},\\vec{y})$ for all $y$ and choose the $K$ words with the highest similarity score. This is defined in the `neighbors` function below:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "outputs": [],
   "source": [
    "def neighbors(query_word, embeddings, K):\n",
    "    # if the query word does not exist in embeddings, return an empty DataFrame\n",
    "    if (query_word not in embeddings.index):\n",
    "        return pd.DataFrame()\n",
    "    # get the vector of the query word (x)\n",
    "    query_vector = embeddings.loc[query_word]\n",
    "    # compute the dot product between each y and the query vector x\n",
    "    similarities = embeddings.dot(query_vector)\n",
    "    # sort the similarity vector by descending similarity value (the most similar value is on top of the vector)\n",
    "    similarities = similarities.sort_values(ascending=False)\n",
    "    # take the items from 1 to K+1\n",
    "    similarities = similarities[1:K+1]\n",
    "    return similarities"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's try out this function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "n = neighbors('hell', vectors[1900], K=5)\n",
    "print(n)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Questions:**\n",
    "- Play with the near-neighbor function by changing the year, the query word and/or the variable $K$ and write down your impressions.\n",
    "- In the second-last line of the `neighbors()` function, we discarded the first item of the similarities vector. (Otherwise, we would have written `similarities = similarities[0:K]`). Why do we want to discard this first item?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's define another function that computes and displays the five nearest neighbors for each year of our dataset:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "outputs": [],
   "source": [
    "def neighbors_per_year(query, all_embeddings, K=5):\n",
    "    for year in sorted(all_embeddings):\n",
    "        nn = neighbors(query, all_embeddings[year], K=K)\n",
    "        print(year, list(nn.index))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Some examples that make use of the convenience function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "neighbors_per_year('hell', vectors)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "neighbors_per_year('gay', vectors)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "neighbors_per_year('hot', vectors)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "**Questions:**\n",
    "- Try to find an example with as many similar neighbors as possible in 1800 and 1990.\n",
    "- Try to find another example with as few similar neighbors as possible in 1800 and 1990."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "source": [
    "## Linking embeddings over time\n",
    "\n",
    "The embedding matrices were constructed by taking the 10000 most frequent words for each year. This list is not quite the same for all years. Therefore, before we can compare two matrices, we need make sure that the rows of both matrices refer to the same words.\n",
    "\n",
    "Some data inspection first:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# tail is the opposite of head - it displays the last rows of the dataframe\n",
    "print(vectors[1900].tail(3))\n",
    "print(vectors[1990].tail(3))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The words are indeed not the same. So if we blindly compare the last row of 1900 with the last row of 1990, we would compare *bags* with *sheriff*, which doesn't make sense.\n",
    "\n",
    "Therefore, we need to align the matrices. Pandas provides a function that does exactly this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# for the rest of the notebook, we'll focus on the contrast between 1900 and 1990\n",
    "year1 = 1900\n",
    "year2 = 1990\n",
    "\n",
    "# 'inner' takes the intersection and discards words that only occur in one vector\n",
    "aligned_y1, aligned_y2 = vectors[year1].align(vectors[year2], join='inner')\n",
    "\n",
    "print(vectors[year1].shape, vectors[year2].shape, aligned_y1.shape, aligned_y2.shape)\n",
    "\n",
    "print(aligned_y1.head().index.values)\n",
    "print(aligned_y2.head().index.values)\n",
    "\n",
    "print(aligned_y1.tail().index.values)\n",
    "print(aligned_y2.tail().index.values)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Questions:**\n",
    "- How many rows did the vectors have before the alignment? How many rows do they have after the alignment?\n",
    "- Are the top rows of the vectors aligned?\n",
    "- Are the bottom rows of the vectors aligned?\n",
    "\n",
    "(The code block above should directly give you the answers to these questions.)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "source": [
    "To validate further, let's check that words in `aligned_y1` have roughly the same near-neighbors as in `aligned_y2`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "outputs": [],
   "source": [
    "for query in ['coffee','gay','variance','peer']:\n",
    "    print(query)\n",
    "    print(year1, neighbors(query, aligned_y1, K=7).index.values)\n",
    "    print(year2, neighbors(query, aligned_y2, K=7).index.values)\n",
    "    print()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "source": [
    "Looks good!\n",
    "\n",
    "Let's have a look at our road map:\n",
    "\n",
    "- Our goal is to automatically find words whose meanings have changed between `year1` and `year2`. In the examples above, we have given an explicit list of words (*coffee, gay, variance, peer*), but we ultimately would like the program to come up with this list by itself.\n",
    "- The alignment procedure above aligns the rows of the matrices, but does not modify the values contained in them (the embeddings properly speaking). We still need to do that - that's the orthogonal Procrustes problem. To avoid terminological confusion, we call this step *projection*."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "source": [
    "## Procrustes\n",
    "$\\newcommand{\\transpose}[1]{#1^{\\top}}$\n",
    "The orthogonal Procrustes problem for matrices $A$ and $B$ is to find an orthogonal projection matrix $\\Omega$ that minimizes the norm of the difference between $\\Omega A$ and $B$:\n",
    "\n",
    "\\begin{equation}\n",
    "\\min_{\\Omega} ||\\Omega A - B ||_F,\n",
    "\\end{equation}\n",
    "s.t. $\\transpose{\\Omega} \\Omega = \\mathbb{I}$, where $A, B \\in \\mathbb{R}^{N \\times V}$ are matrices of $N$-dimensional embeddings, and $\\Omega$ is therefore an orthonormal $N \\times N$ projection matrix. The norm $||M||_F = \\left(\\sum_i \\sum_j m_{i,j}^2\\right)^{1/2}$. \n",
    "\n",
    "The solution to the orthogonal Procrustes problem is obtained by performing singular value decomposition on the product,\n",
    "\n",
    "\\begin{align}\n",
    "U \\Sigma \\transpose{V} &{=} \\text{SVD}(B \\cdot \\transpose{A})\\\\\n",
    "\\Omega &{=} U \\cdot \\transpose{V}\n",
    "\\end{align}\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "This can be translated to Python code in a relatively straightforward way:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "def procrustes(A, B):\n",
    "    U, sigma, Vt = np.linalg.svd(B.dot(A.T))\n",
    "    omega = U.dot(Vt)\n",
    "    return omega"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let $A \\in \\mathbb{R}^{K \\times V}$ be the matrix of aligned word vectors, with words on the columns, and let $B \\in \\mathbb{R}^{K \\times V}$ be an analogous matrix of word vectors at an alternative time. Then we solve the orthogonal Procrustes problem to obtain the projection $\\Omega$.\n",
    "\n",
    "Note that our embedding matrices (`aligned_y1` and `aligned_y2`) don't have the words on the columns, but on the rows. Therefore, we need to transpose them (`.T`) to make the Procrustes analysis work correctly. At the end, we have to transpose the result again to recover the original words-on-rows shape.\n",
    "\n",
    "Let's try that out:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Omega = procrustes(aligned_y1.T, aligned_y2.T)\n",
    "OmegaAlpha = Omega.dot(aligned_y1.T).T\n",
    "projected_y1 = pd.DataFrame(data=OmegaAlpha, index=aligned_y1.index)\n",
    "\n",
    "print(aligned_y1.shape, aligned_y2.shape, projected_y1.shape)\n",
    "print(aligned_y1.head())\n",
    "print(projected_y1.head())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Question:**\n",
    "- What has changed? Are the results as expected?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Another sanity check is to see whether the projection decreases the total difference between the `year1` and `year2` matrices:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('Norm before projection: ', np.linalg.norm(aligned_y2 - aligned_y1))\n",
    "print('Norm after projection:  ', np.linalg.norm(aligned_y2 - projected_y1))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's visualize the embedding differences for the word `fish` (a word which we assume to be semantically stable) with and without projection:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "q_y1_aligned = aligned_y1.loc['fish']\n",
    "q_y1_projected = projected_y1.loc['fish']\n",
    "q_y2_aligned = aligned_y2.loc['fish']\n",
    "\n",
    "# show two plots side by side\n",
    "fig, ax = plt.subplots(1,2, figsize=(10,4))\n",
    "\n",
    "sns.scatterplot(x=q_y2_aligned, y=q_y1_aligned, ax=ax[0])\n",
    "sns.scatterplot(x=q_y2_aligned, y=q_y1_projected, ax=ax[1])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Question:**\n",
    "- Can you describe what happened to the embedding values?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Measuring semantic change by embedding norms\n",
    "\n",
    "We can now finally start searching for words with large changes in meaning.\n",
    "\n",
    "For this, we compute the *cosine distance* between the two embeddings for each word. The function below shows how to do this."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "def compute_cos_dist(vec1, vec2):    \n",
    "    numerator = (vec1 * vec2).sum(axis=1)\n",
    "    denominator = np.sqrt((vec1**2).sum(axis=1)) * np.sqrt((vec2**2).sum(axis=1))\n",
    "    cos_dist = 1 - numerator / (denominator + 0.00001)\n",
    "    return pd.DataFrame(data=cos_dist, index=vec1.index, columns=['CosDist'])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "cos_dist = compute_cos_dist(projected_y1, aligned_y2)\n",
    "print(cos_dist.head())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Let's sort this list and print the words with the biggest changes from 1900 to 1990. \n",
    "\n",
    "The embeddings for rare words are a little weird, so let's focus on the 10000 most frequent before sorting."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cos_dist = cos_dist[:10000].sort_values(by='CosDist', ascending=False)\n",
    "print(cos_dist.head(10))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Questions:**\n",
    "- Can you describe, for each of these words, what their emerging sense is and how it differs from their original sense?\n",
    "- Are there words for which you cannot find a meaning change?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's display the same words together with their 1900 neighbors and their 1990 neighbors."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "for line in cos_dist[:10].iterrows():\n",
    "    print(line[0])\n",
    "    print(\"    Cosine distance: {:.4f}\".format(line[1]['CosDist']))\n",
    "    print(\"    1900 neighbors: \", neighbors(line[0], projected_y1, K=7).index.values)\n",
    "    print(\"    1990 neighbors: \", neighbors(line[0], aligned_y2, K=7).index.values)\n",
    "    print()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "**Question:**\n",
    "- Does this additional information support/invalidate your hypotheses?\n",
    "\n",
    "This concludes the presentation of the *global* / embedding-based / first-order method for the detection of semantic change. Below, we look at the *local* method."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Measuring semantic change by near neighbors\n",
    "\n",
    "A concern with comparing word embeddings directly is that different parts of the embedding space may have different densities, so that cosine distances are not comparable across the entire space. \n",
    "\n",
    "To address this problem, [Hamilton et al (2016b)](https://nlp.stanford.edu/pubs/hamilton2016cultural.pdf) propose a \"local\" technique:\n",
    "\n",
    "1. For word $w$ at time $t$, identify $K$-nearest neighbors $\\mathcal{N}_t(w)$\n",
    "2. Compute the vector of cosine similarities to the union of these neighbors,\n",
    "\n",
    "\\begin{align}\n",
    "\\vec{s}_t(w) = &{} [\\cos(x_{t}(w), x_t(w')) : w' \\in \\mathcal{N}_t(w) \\cup \\mathcal{N}_{t'}(w)]\\\\\n",
    "\\vec{s}_{t'}(w) = &{} [\\cos(x_{t'}(w), x_{t'}(w')) : w' \\in \\mathcal{N}_t(w) \\cup \\mathcal{N}_{t'}(w)]\\\\\n",
    "\\end{align}\n",
    "\n",
    "3. Compute the cosine distance between these two vectors, $D_{t,t'}(w) = 1 - \\cos(\\vec{s}_t(w), \\vec{s}_{t'}(w))$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Intuitively, if a word acquires a large number of new neighbors at $t'$, and these neighbors had very different embeddings from $x_t(w)$ at time $t$, then the distance will be large; if the neighbors and their similarities to $w$ are roughly the same at $t$ and $t'$, then the distance will be small.\n",
    "\n",
    "Let's look at an example."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "query = 'agreed'\n",
    "nn_old = neighbors(query, aligned_y1, K=5)\n",
    "nn_new = neighbors(query, aligned_y2, K=5)\n",
    "print(nn_old.index.values)\n",
    "print(nn_new.index.values)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "We have used `aligned_y1` and `aligned_y2` instead of `vectors[year1]` and `vectors[year2]` to make sure that all words occur in both vectors.\n",
    "\n",
    "Now we'll construct a new dataframe that combines these lists."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "nn_union = nn_old.index.union(nn_new.index)\n",
    "print(nn_union.values)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "For each of these words, we will compute the cosine similarity to the query, in each dataset. \n",
    "\n",
    "Because the vectors are guaranteed to have a unit norm, the cosine similarity is identical to the dot product."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "def get_neighbor_sims(query, neighbor_list, embeddings):\n",
    "    emb_self = embeddings.loc[query]\n",
    "    emb_neighbors = embeddings.loc[neighbor_list]\n",
    "    return pd.DataFrame(data=np.dot(emb_neighbors, emb_self).T, index=neighbor_list, columns=['CosSim'])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "sim_old = get_neighbor_sims(query, nn_union, aligned_y1)\n",
    "sim_new = get_neighbor_sims(query, nn_union, aligned_y2)\n",
    "print(sim_old)\n",
    "print()\n",
    "print(sim_new)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Finally, we'll measure semantic shift by the average \"distance\" between these vectors of similarities. \n",
    "\n",
    "This is a \"second order\" view of semantic change, but we will call it the *local* change, because it is based only on the local neighborhood and not the global embedding space."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "avg_distance = compute_cos_dist(sim_old.T, sim_new.T) / sim_old.size\n",
    "print(avg_distance)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "We'll now make this calculation for the entire dataset.\n",
    "\n",
    "**This will take a few minutes to execute!**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "outputs": [],
   "source": [
    "from tqdm import tqdm_notebook as tqdm # convenient progressbar\n",
    "\n",
    "neighbor_shift = {}\n",
    "num_neighbors = 25\n",
    "\n",
    "for word in tqdm(aligned_y1.index):\n",
    "    nn_old = neighbors(word, aligned_y1, K=num_neighbors)\n",
    "    nn_new = neighbors(word, aligned_y2, K=num_neighbors)\n",
    "    nn_union = nn_old.index.union(nn_new.index)\n",
    "\n",
    "    s1 = get_neighbor_sims(word, nn_union, aligned_y1)\n",
    "    s2 = get_neighbor_sims(word, nn_union, aligned_y2)\n",
    "\n",
    "    dL = compute_cos_dist(s1.T, s2.T) / s1.size\n",
    "    neighbor_shift[word] = dL.iloc[0,0]\n",
    "        \n",
    "df_emb_shift = pd.Series(data=neighbor_shift, name='LocalScore')\n",
    "print(df_emb_shift.head())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "source": [
    "Let's sort the results and look at the 10 words with the highest change:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "print(df_emb_shift.sort_values(ascending=False).head(10))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "**Question:**\n",
    "- At first sight, do these results look better or worse than those of the global method? Are they worth the wait due to the complex computation?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's again display the words together with their neighbors:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "for word in df_emb_shift.sort_values(ascending=False).head(10).index:\n",
    "    print(word)\n",
    "    print('1900 neighbors:', neighbors(word, aligned_y1, K=7).index.values)\n",
    "    print('1990 neighbors:', neighbors(word, aligned_y2, K=7).index.values)\n",
    "    print()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "**Questions:**\n",
    "- Can you describe, for each of these words, what their emerging sense is and how it differs from their original sense?\n",
    "- Are there words that clearly should not end up in this list?\n",
    "- The method above uses a neighborhood size of `25`. Try a number of different neighborhood sizes, and test how it affects the ranking of words. Is the local method sensitive to this parameter? (You can defer this experiment to the end of the tutorial as it is likely to take a lot of time to recompute the local scores.)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "**Further questions related to both the global and the local method:**\n",
    "\n",
    "- Rerun the code by choosing different combinations of `year1` and `year2`. Do the results correspond to your expectations?\n",
    "- It has been argued that words shift their meanings more in the late 20th century. Can you find any evidence of this change of pace?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
