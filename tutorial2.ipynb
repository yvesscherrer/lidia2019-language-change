{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "Happier on the weekend?\n",
    "==========\n",
    "\n",
    "- Jacob Eisenstein, May 2019\n",
    "- Adapted by Yves Scherrer, September 2019\n",
    "\n",
    "One of the early demonstrations of the power of social media to offer observational insights about large populations is the 2011 paper by Scott Golder and Michael Macy, [Diurnal and Seasonal Mood Vary with Work, Sleep, and Daylength Across Diverse Cultures](https://pdfs.semanticscholar.org/d087/5a52e332e716ca84d60d44954c1d9b4f6143.pdf) (G&M).\n",
    "\n",
    "In this paper, G&M collect a large dataset from Twitter and apply the LIWC sentiment analysis model to assign *positive affect* and *negative affect* values to each tweet. They report changes in positive and negative affect by hour, weekday, length of day, and country. They find that people consistently produce more positive tweets on the weekend, and more negative tweets late at night. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "In this notebook, we use data from G&M to attempt to replicate their results concerning hourly and weekly changes. We will not look into geographical and length-of-day effects. We will also use **multiple regression**, a statistical model that can identify the impact of multiple predictors on an outcome variable."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# A first look at the data\n",
    "\n",
    "As in last week's tutorial, the data is provided in the form of a CSV file. You can [click on this link](data/golder_macy_prefix.txt) to inspect the data file. The data file does not contain the original tweets, only some numeric representation of them. \n",
    "\n",
    "To access the data, we will load it as a `pandas` dataframe:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "import pandas as pd\n",
    "# the 'as pd' part will allow us to refer to the pandas module by the pd shortcut\n",
    "\n",
    "# in this file, columns are separated by tabs rather than commas\n",
    "# this is specified in the 'delimiter' part\n",
    "df = pd.read_csv('data/golder_macy_prefix.txt', delimiter='\\t')\n",
    "\n",
    "# display the first lines of the data frame\n",
    "print(df.head())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "Each row is a single record, corresponding to a tweet, with the following fields:\n",
    "- **hour**, which is computed per week, so that the maximum value is $24 * 7 - 1 = 167$\n",
    "- **pa**: fraction of tokens in the tweet matching the **positive affect** lexicon in [LIWC](https://www.cs.cmu.edu/~ylataus/files/TausczikPennebaker2010.pdf)\n",
    "- **na**: fraction of tokens matching LIWC's **negative affect** lexicon\n",
    "- **uid**: a user identifier\n",
    "\n",
    "Pandas provides easy ways to find the maximum and minimum values per column. This is useful as a sanity check:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "outputs": [],
   "source": [
    "print(\"Min values:\")\n",
    "print(df.min())\n",
    "print()\n",
    "print(\"Max values:\")\n",
    "print(df.max())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "G&M treat positive and negative sentiment separately. But to simplify the discussion, let's aggregate them into a \"sentiment ratio\". The sentiment ratio is defined as follows: $\\frac{PA}{PA + NA}$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "df['ratio'] = df['pa'] / (df['pa'] + df['na'] + 0.0000000001)\n",
    "print(df.head())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can see that a new column called `ratio` has been created in the dataframe."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Question:**\n",
    "- What is the purpose of `+ 0.0000000001`?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "# Plotting the data\n",
    "\n",
    "We want to test the following hypotheses:\n",
    "\n",
    "- People are happier on the weekend\n",
    "- People are less happy late at night\n",
    "\n",
    "As a first step, let's plot the data. To do this, we use the `seaborn` library again."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "outputs": [],
   "source": [
    "# same import statements as last week, adding shortcuts 'plt' and 'sns'\n",
    "import matplotlib.pyplot as plt\n",
    "%matplotlib inline\n",
    "import seaborn as sns"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "First, let's make a scatterplot between hour and sentiment ratio. In other words, let's have the hours as the `x` axis and the sentiment ratio as the `y` axis and put a dot for each observation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "sns.scatterplot(x='hour', y='ratio', data=df)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "source": [
    "That doesn't seem to be particularly useful...\n",
    "\n",
    "Let's try a different type of visualisation, a line plot:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "sns.lineplot(x='hour', y='ratio', data=df)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Better! `seaborn`'s `lineplot` automatically computes the mean sentiment for each hour of the week and displays that. The light blue areas represent a single standard deviation by default.\n",
    "\n",
    "The eye test suggests that there is some structure in this data. Let's try and tease out time-of-day and day-of-week effects. We'll do this by adding additional fields/columns to the data frame."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "# Integer division: if you divide 87 by 24, you get 3 and a rest of 15\n",
    "# This means that hour 87 is the 15th hour of day 3\n",
    "# We'll store the value 3 in day_of_week and the value 15 in time_of_day\n",
    "df['time_of_day'] = df['hour'] % 24\n",
    "df['day_of_week'] = df['hour'] // 24\n",
    "print(df.head())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`time_of_day` represents the hour of the day (e.g. `11` for all tweets written between 11:00 and 11:59), and `day_of_week` represents the day of the week (`0` for Monday, `6` for Sunday). Let's create a new field/column `day_name` to include the names."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "days = {0: 'Monday', 1: 'Tuesday', 2: 'Wednesday', 3: 'Thursday', 4: 'Friday', 5: 'Saturday', 6: 'Sunday'}\n",
    "df['day_name'] = df['day_of_week'].map(days)\n",
    "df.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's draw a line plot now for `time_of_day` to see averages over all week days:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "sns.lineplot(x='time_of_day', y='ratio', data=df)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "**Questions:**\n",
    "\n",
    "- At what time of the day are people the happiest?\n",
    "- And at what time of the day are they particularly unhappy?\n",
    "- Do these findings confirm one of the initial hypotheses?\n",
    "- To tease apart positive and negative affect, you can create a plot with the `pa` or `na` value instead of `ratio`. Can you spot periods where people have most contradictory feelings (high `pa` as well as `na` values)?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    " Now let's look at the days of the week:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "sns.lineplot(x='day_of_week', y='ratio', data=df)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "**Questions:**\n",
    "\n",
    "- Why did I choose the column `day_of_week` and not `day_name` for the x-axis? What would the plot look like with the latter?\n",
    "- On which weekdays are people the happiest / the unhappiest? Compare again the plots for `ratio`, `pa` and `na` and comment any differences in the outcome.\n",
    "- Do these findings confirm the initial hypothesis?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There seems to be some interaction between the hour of the day and the day of the week. Seaborn allows us to create a plot with one line for each day of the week. (The parameter `ci=None` turns off the light-colored error bands to make the plot more readable.)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sns.lineplot(x='time_of_day', y='ratio', hue='day_name', data=df, ci=None)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This figure is still a bit hard to read. Maybe we can limit the display to one typical weekday (Tuesday) and one typical weekend day (Saturday)?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sns.lineplot(x='time_of_day', y='ratio', hue='day_name', hue_order=['Tuesday', 'Saturday'], data=df)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Better, but we've discarded most of the data from the plot. Maybe we can aggregate all weekdays and all days of the weekend? Let's add another field:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df['weekday'] = df['day_of_week'] < 5\n",
    "print(df.head())"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sns.lineplot(x='time_of_day', y='ratio', hue='weekday', data=df)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Questions:**\n",
    "- G&M suggest that \"people may be emotionally 'refreshed' by sleep\". Can you find any evidence for this suggestion in the above graph?\n",
    "- Do people's presumed sleep patterns differ between weekdays and weekends?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Some statistics...\n",
    "\n",
    "It looks like `time_of_day` as well as `weekday` somehow influence the sentiment ratio. In this section, we will use some statistical modelling to tease apart these two variables.\n",
    "\n",
    "**Multiple regression** is a statistical model of the relationship between a set of **predictors** (independent variables) and an **outcome** (dependent variable). A linear regression can be specified through the following canonical notation:\n",
    "\n",
    "$ Y \\sim X_1 + X_2 + \\ldots + X_N.$\n",
    "\n",
    "This states that $Y$ is the outcome, in our case the sentiment ratio. It will regress against the predictors $X_1, X_2, \\ldots, X_N$. In our case, these correspond to the different hours of the day and to the weekday/weekend distinction. Specifically, we will estimate coefficients for each $X_i$. The same notation can be used to specify more complicated regression models, including dummy variables and interactions."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Multiple regression in Python\n",
    "\n",
    "In Python, regressions can be specified and performed using the `statsmodels` module, which uses a similar syntax to R's `glmnet`. Let's first import the module."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "import statsmodels.formula.api as smf"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "We first have to write the regression formula, replacing the $X$ and $Y$ by the variable names of our dataframe:\n",
    "- The outcome is `ratio`\n",
    "- We would like to have a predictor for each possible value of `time_of_day`, e.g. for each hour. This can be done using the notation `C(time_of_day)`.\n",
    "- We would like to have an additional predictor for whether the day is a weekday: `weekday`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "regr_formula = 'ratio ~ C(time_of_day) + weekday'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Now let's fit a regression, using ordinary least squares:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "model = smf.ols(formula=regr_formula, data=df)\n",
    "results = model.fit()\n",
    "print(results.summary())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Understanding the regression results\n",
    "\n",
    "This huge table needs some explanation.\n",
    "- The upper part of the summary includes some statistics about the goodness-of-fit, including `R-squared`, `F-statistic`, `AIC` and `Log-Likelihood`. These statistics can help to decide whether it is justified to use more complicated models.\n",
    "- Some of the coefficients are negative, some are positive. The night hours (1-6) contribute negatively to sentiment ratio, the morning hours (9-12) contribute most positively. The coefficient for `weekday` is negative, meaning that weekdays show lower sentiment ratio than weekends.\n",
    "- The **standard error** of each coefficient quantifies the uncertainty due to the dataset size.\n",
    "- The $t$ value is the coefficient divided by the standard error.\n",
    "- Under the **null hypothesis**, we would expect the distribution of each $t$ value to follow a Student's $t$ distribution, with mean $0$, and variance approaching $1$ as the dataset size grows. Suppose a coefficient has $t = \\tau$. The **statistical significance** is based on the probability of having $|t| \\geq |\\tau|$ under the null hypothesis. This is the $p$ value, written $P > |t|$ in the summary. It is customary to reject the null hypothesis when $p < 0.05$.\n",
    "- According to this definition, about half of the coefficients for `time_of_day` are significant (i.e. have $p < 0.05$), and the `weekday` feature is significant as well."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "source": [
    "### Bonferroni correction\n",
    "\n",
    "However, a correction is required, because we are performing multiple significance tests at the same time. This overestimates statistical significance.\n",
    "\n",
    "The **Bonferroni correction** suggests that a more stringent significance level is more appropriate in this case: the customary significance level has to be divided by the number of predictors. In our case: $0.05 / 25 = 0.002$.\n",
    "\n",
    "As a result, only a few coefficients \"survive\" this correction: one or two hours during the night, and two hours during the morning show p-values below the corrected level.\n",
    "\n",
    "The `weekday` variable does not survive the correction either. Do you have an idea what could make this variable fit better?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "source": [
    "### Multi-collinearity\n",
    "\n",
    "You may have noted that there is no predictor for \"hour 0\", which would be `C(time_of_day)[T.0]`. In fact, this predictor is completely determined by the other `time_of_day` variables. The same is true for `weekday[T.False]` with respect to `weekday[T.True]`.\n",
    "\n",
    "Including these predictors would make the estimation problem underdetermined: we could add a constant to all `time_of_day` predictors, and subtract the same constant from the `intercept` term. In regression, this is called **multi-collinearity**. It is impossible to interpret regression coefficients when the predictors are closely correlated.\n",
    "\n",
    "This isn't a problem for prediction, only for explanation, which is why closely related variables can be used in machine learning without concern."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Visualizing the regression results\n",
    "\n",
    "Let's plot the coefficients for the time-of-day predictors, along with their standard errors."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "hours = range(1,24)\n",
    "hour_coeffs = [0] + [results.params[f'C(time_of_day)[T.{hour}]'] for hour in hours]\n",
    "hour_ses = [0] + [results.bse[f'C(time_of_day)[T.{hour}]'] for hour in hours]\n",
    "hours = [0] + list(hours)\n",
    "\n",
    "# create a line plot with error bars\n",
    "plt.errorbar(hours, hour_coeffs, yerr=hour_ses)\n",
    "# horizontal red line at 0\n",
    "plt.hlines(0, xmin=-1, xmax=hours[-1]+1, color='r', alpha=0.5)\n",
    "plt.xlabel('hour')\n",
    "plt.ylabel('sentiment coefficient')\n",
    "plt.title('regression results');"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For comparison, this is again the first time-of-day plot we have drawn:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sns.lineplot(x='time_of_day', y='ratio', data=df)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Question:**\n",
    "- What are the similarities and differences between these two plots?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Interactions between variables\n",
    "\n",
    "As we have seen above, there is some interaction between `time_of_day` and `weekday`: some hours may be predictive for a particular sentiment only on weekdays or only on weekends. The multiple regression analysis above does not take into account such interactions.\n",
    "\n",
    "One simple way of adding this is to duplicate the `time_of_day` predictor variables and create one version for weekdays and one for weekends. This can be done with the `:` operator in the regression formula:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "regr_formula = 'ratio ~ C(time_of_day) : weekday'\n",
    "model = smf.ols(formula=regr_formula, data=df)\n",
    "results = model.fit()\n",
    "print(results.summary())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This will have the effect that the p-values will further increase, simply because there are fewer data points available for each category. However, we can spot that the most reliably positive and negative sentiments (in terms of coefficients as well as p-value) occur in different time periods on weekdays and on weekends.\n",
    "\n",
    "More sophisticated analyses could be carried out with **mixed effects** models."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Final question:**\n",
    "- The dataset contains an additional column `uid`, which we haven't used at all here. What kinds of analysis could be done with this information? Could the inclusion of user information increase the reliability of the results obtained for the addressed research questions?"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
