{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Chameleons in imagined conversations\n",
    "\n",
    "- Yves Scherrer, October 2019\n",
    "\n",
    "In this tutorial, we are going to look at linguistic change occurring within conversations. It has been shown that **accommodation** or **coordination** occurs between the different participants of a conversation, i.e. that they unconciously adapt to each other in terms of linguistic style. [Danescu-Niculescu-Mizil et al. (2011)](http://www.ramb.ethz.ch/CDstore/www2011/proceedings/p745.pdf) show that linguistic coordination also appears in non-simultaneous written conversations on Twitter.\n",
    "\n",
    "In their follow-up research, [Danescu-Niculescu-Mizil & Lee (2011)](https://www.aclweb.org/anthology/W11-0609.pdf) look at conversations extracted from movies. This type of data is relatively easy to collect and to analyze, but it is scripted and therefore may differ considerably from spontaneous conversations. In this tutorial, we well see to what extent coordination also occurs in scripted dialogs."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## A first look at the data\n",
    "\n",
    "The data is provided in several CSV files. The first file, `movie-corpus-utterances.csv.gz`, contains one utterance per row with the related metadata. This file is compressed, but pandas will automatically uncompress it when loading."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pandas as pd\n",
    "import matplotlib.pyplot as plt\n",
    "%matplotlib inline\n",
    "import seaborn as sns"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "df_utts = pd.read_csv('data/movie-corpus-utterances.csv.gz')\n",
    "print(df_utts.head())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "Each row represents an utterances with its content and metadata:\n",
    "- **speaker**: the name of the speaker producing the utterance (name of the movie character + movie id)\n",
    "- **gender**: the speaker's gender (*f, m* or _?_)\n",
    "- **utt_id**: the unique identifier of the utterance\n",
    "- **utt_text**: the content of the utterance\n",
    "- **adverb, article, auxverb, conj**: whether the utterance contains adverbs, articles, auxiliary verbs, conjunctions (1 or 0)\n",
    "- **ipron, ppron, ppreps, quant**: whether the utterance contains indefinite pronouns, personal pronouns, prepositions, quantifiers (1 or 0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us now load the second file, `movie-corpus-pairs.csv`, into a dataframe:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df_pairs = pd.read_csv('data/movie-corpus-pairs.csv')\n",
    "print(df_pairs.head())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "Each row represents a pair of adjacent utterances $\\langle A, B \\rangle$, where the first utterance $A$ (the *target*) is produced by speaker $S_A$ and the second utterance $B$ (the *reply*) is produced by speaker $S_B$. The identifiers refer to those of the previous dataframe.\n",
    "\n",
    "For instance, the first row of this table says that *L1044* is followed by *L1045*, i.e. that the utterance *They do to!* (produced by *CAMERON_m0*) is followed by the utterance *They do not!* (produced by *BIANCA_m0*)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Having the information spread out over two dataframes is not very convenient. We will now add the information contained in `df_utts` into `df_pairs`. This will happen in two steps, first by adding the information for utterance $A$ and second for utterance $B$:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# add columns for utterance A\n",
    "df_pairs_merged = df_pairs.merge(df_utts, left_on='utt_id_A', right_on='utt_id')\n",
    "# add columns for utterance B, rename the previously merged columns to *_A and the newly merged columns to *_B\n",
    "df_pairs_merged = df_pairs_merged.merge(df_utts, left_on='utt_id_B', right_on='utt_id', suffixes=('_A', '_B'))\n",
    "# as a result, the columns utt_id_A and utt_id_B are duplicated - the following command removes them:\n",
    "df_pairs_merged = df_pairs_merged.loc[:,~df_pairs_merged.columns.duplicated()]\n",
    "print(df_pairs_merged.head())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now that we have all information in one place, we have to reformat the dataframe once more so that we can compute relative frequencies more easily. The goal is to have 8 rows per utterance pair (because there are 8 linguistic features), and two columns showing whether the feature in question is realized in utterances A and B."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "outputs": [],
   "source": [
    "# create a dataframe for all *_A values of the linguistic features\n",
    "# we keep the metadata for both A and B (speaker_*, gender_*, utt_id_*), but remove text_* as we are not going to use it\n",
    "df_A = df_pairs_merged.melt(id_vars=['speaker_A', 'speaker_B', 'gender_A', 'gender_B', 'utt_id_A', 'utt_id_B'],\n",
    "              value_vars=['adverb_A', 'article_A', 'auxverb_A', 'conj_A', 'ipron_A', 'ppron_A', 'preps_A', 'quant_A'],\n",
    "              var_name='feature', value_name='value_A')\n",
    "# Convert e.g. 'adverb_A' => 'adverb'\n",
    "df_A['feature'] = df_A['feature'].str[:-2]\n",
    "\n",
    "# create a dataframe for all *_B values of the linguistic features\n",
    "# we only keep utt_id_A and utt_id_B as metadata as they uniquely identify the utterance pair\n",
    "df_B = df_pairs_merged.melt(id_vars=['utt_id_A', 'utt_id_B'],\n",
    "              value_vars=['adverb_B', 'article_B', 'auxverb_B', 'conj_B', 'ipron_B', 'ppron_B', 'preps_B', 'quant_B'],\n",
    "              var_name='feature', value_name='value_B')\n",
    "# Convert e.g. 'adverb_B' => 'adverb'\n",
    "df_B['feature'] = df_B['feature'].str[:-2]\n",
    "\n",
    "# merge df_A and df_B back together matching columns utt_id_A, utt_id_b and feature\n",
    "# we only merge those rows of df_A and df_B which have the same feature\n",
    "df_M = pd.merge(df_A, df_B, how='inner', on=['utt_id_A', 'utt_id_B', 'feature'])\n",
    "print(df_M.head())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us add a column `value_A_and_B` that contains 1 if both `value_A` and `value_B` are 1."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df_M['value_A_and_B'] = df_M['value_A'] * df_M['value_B']\n",
    "print(df_M.head())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Data aggregation\n",
    "\n",
    "Let us now aggregate the data by speaker pair and feature. For each speaker pair $\\langle S_A, S_B \\rangle$ and linguistic feature $t$, we would like to compute the following numbers:\n",
    "- the number of utterance pairs $\\langle A, B \\rangle$ where $t \\in B$ (i.e. $B$ contains $t$)\n",
    "- the number of utterance pairs $\\langle A, B \\rangle$ where $t \\in A \\land t \\in B$ (i.e. both $A$ and $B$ contain $t$)\n",
    "- the total number of utterance pairs between $S_A$ and $S_B$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# use the original dataframe to count the number of rows per pair of speakers\n",
    "df_totals = df_pairs_merged.groupby(['speaker_A', 'speaker_B']).size()\n",
    "print(df_totals.head())\n",
    "\n",
    "# use the modified dataframe to aggregate the feature-wise counts per pair of users\n",
    "df_counts = df_M.groupby(['speaker_A', 'speaker_B', 'gender_A', 'gender_B', 'feature']).agg('sum')\n",
    "\n",
    "# add the totals as a separate column to df_counts\n",
    "df_counts['total'] = df_totals\n",
    "print(df_counts.head())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Coordination** can be measured with the **convergence score**. The convergence score $C$ between two speakers $S_A$ and $S_B$ on feature $t$ is defined as the difference between two probabilities:\n",
    "\n",
    "$$C(S_A, S_B, t) = P(t \\in B | t \\in A \\land A \\hookleftarrow B) - P(t \\in B | A \\hookleftarrow B)$$\n",
    "\n",
    "where $A \\hookleftarrow B$ means that utterance $B$ is a reply to utterance $A$.\n",
    "\n",
    "The second probability can be computed like this:\n",
    "\n",
    "$$P(t \\in B | A \\hookleftarrow B) = \\frac{\\mathrm{value\\_B}}{\\mathrm{total}}$$\n",
    "\n",
    "The first probability is a bit more difficult to compute. We first make use of the probabilistic chain rule to eliminate the conditional probability:\n",
    "\n",
    "$$P(t \\in B | t \\in A \\land A \\hookleftarrow B) = \\frac{P(t \\in B \\land t \\in A | A \\hookleftarrow B)}{P(t \\in A | A \\hookleftarrow B)}$$\n",
    "\n",
    "The numerator is computed as follows:\n",
    "\n",
    "$$P(t \\in B \\land t \\in A | A \\hookleftarrow B) = \\frac{\\mathrm{value\\_A\\_and\\_B}}{\\mathrm{total}}$$\n",
    "\n",
    "and the denominator as follows:\n",
    "\n",
    "$$P(t \\in A | A \\hookleftarrow B) = \\frac{\\mathrm{value\\_A}}{\\mathrm{total}}$$\n",
    "\n",
    "We add one column for each of these computations to the dataframe:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "df_counts['prob_B'] = df_counts['value_B'] / df_counts['total']\n",
    "df_counts['prob_A_and_B'] = df_counts['value_A_and_B'] / df_counts['total']\n",
    "df_counts['prob_A'] = df_counts['value_A'] / df_counts['total']\n",
    "df_counts['prob_B_given_A'] = (df_counts['prob_A_and_B'] / df_counts['prob_A'])\n",
    "df_counts['convergence'] = df_counts['prob_B_given_A'] - df_counts['prob_B']\n",
    "print(df_counts.head())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Visualization\n",
    "\n",
    "Let us try to replicate the bar plots of the paper. Note that these plots don't directly show the convergence scores, but rather the two probabilities side by side.\n",
    "\n",
    "In order to make this work, we have to reformat our last dataframe again. We will duplicate each row so that one instance shows the `prob_B_given_A` score and the other one shows the `prob_B` score. The `barplot` function will then automatically compute the average probabilities over all speaker pairs and plot them:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "outputs": [],
   "source": [
    "df_temp = df_counts.reset_index().melt(id_vars=['speaker_A', 'speaker_B', 'feature'], value_vars=['prob_B_given_A', 'prob_B'])\n",
    "print(df_temp.head())\n",
    "\n",
    "sns.barplot(x='feature', y='value', hue='variable', data=df_temp)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Questions**:\n",
    "- Are the average **convergence scores** positive or negative? In which range are they?\n",
    "- How do these results compare to the results on Twitter data shown in the slides? Do they match your expectations?\n",
    "- Seaborn automatically draws error bars. A simple way of gauging statistical significance is to see whether the error bars overlap. If they do not, the difference is likely to be statistically significant. What is your overall impression? If you need to increase the plot size, you can add the line `plt.figure(figsize=(10, 10))` before the `sns.barplot(...)` line."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Sanity check 1: swap A and B\n",
    "\n",
    "One way to check whether the results are reliable is to contrast them with a setup in which we do not expect convergence. One such setup can be obtained simply by swapping the two utterances $A$ and $B$ and to measure how much $B$ influences $A$. Since $A$ is uttered before $B$, there should not be any such convergence.\n",
    "\n",
    "Convergence from $B$ to $A$ is measured by the probabilities $P(t \\in A | t \\in B \\land A \\hookleftarrow B)$ and $P(t \\in A | A \\hookleftarrow B)$. Let us take the `df_counts` dataframe and add the missing computations:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df_counts['prob_A_given_B'] = df_counts['prob_A_and_B'] / df_counts['prob_B']\n",
    "df_counts['swap_convergence'] = df_counts['prob_A_given_B'] - df_counts['prob_A']\n",
    "print(df_counts.head())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us now create the same type of bar plot:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df_temp = df_counts.reset_index().melt(id_vars=['speaker_A', 'speaker_B', 'feature'], value_vars=['prob_A_given_B', 'prob_A'])\n",
    "sns.barplot(x='feature', y='value', hue='variable', data=df_temp)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The results can be compared more easily if the convergence values are plotted in the same graph. Let's do this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df_temp = df_counts.reset_index().melt(id_vars=['speaker_A', 'speaker_B', 'feature'],\n",
    "                                       value_vars=['convergence', 'swap_convergence'])\n",
    "sns.barplot(x='feature', y='value', hue='variable', data=df_temp)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Questions:**\n",
    "- How does convergence in swapped data differ from convergence in the correctly ordered data? Does this difference correspond to your expectations?\n",
    "- Do you think that 'real', non-scripted data would lead to the same results? Why/why not? "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Sanity check 2: Within-conversation effects\n",
    "\n",
    "How much are the accommodation effects found above due to an immediate triggering effect, as opposed to simply being a by-product of utterances occurring within the same conversation? For instance, could the results be due just to the topic of the conversation?\n",
    "\n",
    "This question can be answered by measuring \"convergence\" between utterances that are **not adjacent**, but are still in the same conversation. For example, in a conversation consisting of the utterance sequence $A_1 B_2 A_3 B_4 A_5$, we will look at utterance pairs $A_1 B_4$ and $B_2 A_5$. This helps control for topic effects, since both utterances of the pair are still\n",
    "close and thus fairly likely to be on the same subject.\n",
    "\n",
    "The list of non-adjacent utterance pairs is available in the file `movie-corpus-nonadj-pairs.csv`. Let's load it into a dataframe:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df_nonadj_pairs = pd.read_csv('data/movie-corpus-nonadj-pairs.csv')\n",
    "print(df_nonadj_pairs.head())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us just go through the whole formatting, aggregation and visualization process again using the new dataset:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# merge df_nonadj_pairs with df_utts\n",
    "df_nonadj_pairs_merged = df_nonadj_pairs.merge(df_utts, left_on='utt_id_A', right_on='utt_id')\n",
    "df_nonadj_pairs_merged = df_nonadj_pairs_merged.merge(df_utts, left_on='utt_id_B', right_on='utt_id', suffixes=('_A', '_B'))\n",
    "df_nonadj_pairs_merged = df_nonadj_pairs_merged.loc[:,~df_nonadj_pairs_merged.columns.duplicated()]\n",
    "print(df_nonadj_pairs_merged.head())"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# reformat the dataframe\n",
    "df_nonadj_A = df_nonadj_pairs_merged.melt(id_vars=['speaker_A', 'speaker_B', 'gender_A', 'gender_B', 'utt_id_A', 'utt_id_B'],\n",
    "              value_vars=['adverb_A', 'article_A', 'auxverb_A', 'conj_A', 'ipron_A', 'ppron_A', 'preps_A', 'quant_A'],\n",
    "              var_name='feature', value_name='value_A')\n",
    "df_nonadj_A['feature'] = df_nonadj_A['feature'].str[:-2]\n",
    "\n",
    "df_nonadj_B = df_nonadj_pairs_merged.melt(id_vars=['utt_id_A', 'utt_id_B'],\n",
    "              value_vars=['adverb_B', 'article_B', 'auxverb_B', 'conj_B', 'ipron_B', 'ppron_B', 'preps_B', 'quant_B'],\n",
    "              var_name='feature', value_name='value_B')\n",
    "df_nonadj_B['feature'] = df_nonadj_B['feature'].str[:-2]\n",
    "\n",
    "df_nonadj_M = pd.merge(df_nonadj_A, df_nonadj_B, how='inner', on=['utt_id_A', 'utt_id_B', 'feature'])\n",
    "df_nonadj_M['value_A_and_B'] = df_nonadj_M['value_A'] * df_nonadj_M['value_B']\n",
    "print(df_nonadj_M.head())"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# sum feature counts by utterance pair and compute probabilities\n",
    "df_nonadj_totals = df_nonadj_pairs_merged.groupby(['speaker_A', 'speaker_B', 'gender_A', 'gender_B']).size()\n",
    "df_nonadj_counts = df_nonadj_M.groupby(['speaker_A', 'speaker_B', 'gender_A', 'gender_B', 'feature']).agg('sum')\n",
    "\n",
    "df_nonadj_counts['total'] = df_nonadj_totals\n",
    "df_nonadj_counts['prob_B'] = df_nonadj_counts['value_B'] / df_nonadj_counts['total']\n",
    "df_nonadj_counts['prob_A_and_B'] = df_nonadj_counts['value_A_and_B'] / df_nonadj_counts['total']\n",
    "df_nonadj_counts['prob_A'] = df_nonadj_counts['value_A'] / df_nonadj_counts['total']\n",
    "df_nonadj_counts['prob_B_given_A'] = df_nonadj_counts['prob_A_and_B'] / df_nonadj_counts['prob_A']\n",
    "df_nonadj_counts['convergence'] = df_nonadj_counts['prob_B_given_A'] - df_nonadj_counts['prob_B']\n",
    "\n",
    "print(df_nonadj_counts.head())"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# final reformatting and visualisation\n",
    "df_nonadj_temp = df_nonadj_counts.reset_index().melt(id_vars=['speaker_A', 'speaker_B', 'feature'],\n",
    "                                                     value_vars=['prob_B_given_A', 'prob_B'])\n",
    "print(df_nonadj_temp.head())\n",
    "sns.barplot(x='feature', y='value', hue='variable', data=df_nonadj_temp)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's plot the convergence values next to each other:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df_merged_counts = df_counts[['convergence']].merge(df_nonadj_counts[['convergence']], on=['speaker_A', 'speaker_B', 'feature'],\n",
    "                                                    suffixes=('_adj', '_nonadj'))\n",
    "print(df_merged_counts.head())\n",
    "df_merged_temp = df_merged_counts.reset_index().melt(id_vars=['speaker_A', 'speaker_B', 'feature'],\n",
    "                                              value_vars=['convergence_adj', 'convergence_nonadj'])\n",
    "sns.barplot(x='feature', y='value', hue='variable', data=df_merged_temp)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Questions:**\n",
    "- What does this plot show?\n",
    "- Does it look different from the comparison plot of sanity check 1? Can you explain these differences?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Gender differences\n",
    "\n",
    "In real-life conversations, it has been observed that the gender of the two speakers affects coordination (Ireland & Pennebacker 2010, Bilous & Krauss 1988). Let us see if such effects carry on in imagined/scripted conversations.\n",
    "\n",
    "We will look at the following four configurations of pairs of adjacent utterances: `f2f`, `f2m`, `m2f`, `m2m`. While the original dataframes contained gender information, we have lost them on the way... The last dataframe where gender was present (before we started doing sanity checks) is `df_counts`, so let's start there and create an additional column with the gender configuration. We'll also remove those rows where the gender configuration does not correspond to one of the four values listed above:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df_counts = df_counts.reset_index()\n",
    "df_counts['gender_pair'] = df_counts['gender_A'] + \"2\" + df_counts['gender_B']\n",
    "df_counts = df_counts.loc[df_counts['gender_pair'].str.contains('[mf]2[mf]')]\n",
    "print(df_counts.head())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We now plot the coordination values for one single feature, namely `article`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df_temp = df_counts.melt(id_vars=['speaker_A', 'speaker_B', 'gender_pair', 'feature'], value_vars=['convergence'])\n",
    "print(df_temp.head())\n",
    "\n",
    "sns.barplot(x='gender_pair', y='value', data=df_temp[df_temp['feature'] == 'article'])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Questions:**\n",
    "- Can you see some trends on the basis of this figure?\n",
    "- Is there more coordination when the *target* is uttered by a man or a woman?\n",
    "- Is there more coordination when the *reply* is uttered by a man or a woman?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us now create a graph that includes all features."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.figure(figsize=(10, 6))\n",
    "# ci=None turns off the error bars to make the plot more readable\n",
    "sns.barplot(x='feature', y='value', hue='gender_pair', data=df_temp, ci=None)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Questions:**\n",
    "- Do your findings for the *article* feature carry over to the other features? Can you draw any conclusions from this final plot?\n",
    "- Throughout this tutorial, we have used somewhat simple linguistic features, i.e. whether an utterance contains articles or not. Suggest some additional features (or modifications of the existing ones) that could give a more precise account of linguistic coordination. You can also suggest features that you would use for a replication study in another language."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Appendix: How the dataset was created\n",
    "\n",
    "This part of the notebook explains how the data files used above were created. This is just for information and future reference. You are not supposed to run this code (you would have to install the `convokit` module)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Load modules:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import csv, gzip\n",
    "import convokit"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Download and load the corpus, then annotate the corpus with LIWC features:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "corpus = convokit.Corpus(filename=convokit.download(\"movie-corpus\"))\n",
    "coord = convokit.Coordination()\n",
    "coord.fit(corpus)\n",
    "print(\"done\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Determine annotation categories and user metadata:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "liwc_categories = set()\n",
    "for ut in corpus.get_utterance_ids():\n",
    "    if 'liwc-categories' in corpus.utterances[ut].meta:\n",
    "        liwc_categories.update(corpus.utterances[ut].meta['liwc-categories'])\n",
    "liwc_categories = sorted(liwc_categories)\n",
    "print(\"Features:\", liwc_categories)\n",
    "\n",
    "userinfo = {}\n",
    "for user in corpus.iter_users():\n",
    "    userinfo[user.name] = user.meta\n",
    "print(\"Users:\", len(userinfo))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Write the annotated utterances to a file:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "outfile = gzip.open(\"movie-corpus-utterances.csv.gz\", 'wt', newline='')\n",
    "outwriter = csv.writer(outfile)\n",
    "\n",
    "titlerow = [\"speaker\", \"gender\", \"utt_id\", \"utt_text\"]\n",
    "for feat in liwc_categories:\n",
    "    titlerow.append(feat)\n",
    "outwriter.writerow(titlerow)\n",
    "\n",
    "rows = 0\n",
    "for uttid in corpus.get_utterance_ids():\n",
    "    userid = corpus.utterances[uttid].user.name\n",
    "    speakername = userinfo[userid]['character_name']\n",
    "    speakergender = userinfo[userid]['gender'].lower()\n",
    "    utt_text = corpus.utterances[uttid].text\n",
    "    movieid = corpus.utterances[uttid].meta['movie_id']\n",
    "    row = [speakername + \"_\" + movieid, speakergender, uttid, utt_text]\n",
    "    for feat in liwc_categories:\n",
    "        row.append(int(feat in corpus.utterances[uttid].meta[\"liwc-categories\"]))\n",
    "    outwriter.writerow(row)\n",
    "    rows += 1\n",
    "    \n",
    "outfile.close()\n",
    "print(\"done,\", rows, \"data rows written to file\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Find pairwise exchanges and write utterance ids to file:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# use parts of Coordination.pairwise_scores() and Coordination.scores_over_utterances()\n",
    "\n",
    "pairs = corpus.speaking_pairs()\n",
    "pairs_utts = corpus.pairwise_exchanges(lambda x, y: (x, y) in pairs, user_names_only=False)\n",
    "print(\"Exchange pairs:\", len(pairs_utts))\n",
    "\n",
    "outfile = open(\"movie-corpus-pairs.csv\", 'w')\n",
    "outwriter = csv.writer(outfile)\n",
    "\n",
    "titlerow = [\"utt_id_A\", \"utt_id_B\"]\n",
    "outwriter.writerow(titlerow)\n",
    "\n",
    "rows = 0\n",
    "for (speaker, target), utterances in pairs_utts.items():\n",
    "    for u2 in utterances:\n",
    "        if u2.reply_to in corpus.utterances:\n",
    "            u1 = corpus.utterances[u2.reply_to]\n",
    "            if speaker != target:    # self-replies account for ~1000 data points - not enough to keep them for separate modeling\n",
    "                row = [u1.id, u2.id]\n",
    "                outwriter.writerow(row)\n",
    "                rows += 1\n",
    "print(\"done,\", rows, \"data rows written to file\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Find non-adjacent pairs starting from the same conversations:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pairs = corpus.speaking_pairs()\n",
    "pairs_utts = corpus.pairwise_exchanges(lambda x, y: (x, y) in pairs, user_names_only=False)\n",
    "print(\"Exchange pairs:\", len(pairs_utts))\n",
    "\n",
    "outfile = open(\"movie-corpus-nonadj-pairs.csv\", 'w')\n",
    "outwriter = csv.writer(outfile)\n",
    "\n",
    "titlerow = [\"utt_id_A\", \"utt_id_B\"]\n",
    "outwriter.writerow(titlerow)\n",
    "\n",
    "rows = 0\n",
    "for (speaker, target), utterances in pairs_utts.items():\n",
    "    for u4 in utterances:\n",
    "        if u4.reply_to in corpus.utterances:\n",
    "            u3 = corpus.utterances[u4.reply_to]\n",
    "            if u3.reply_to in corpus.utterances:\n",
    "                u2 = corpus.utterances[u3.reply_to]\n",
    "                if u2.reply_to in corpus.utterances:\n",
    "                    u1 = corpus.utterances[u2.reply_to]\n",
    "                    newtarget = u1.user\n",
    "                    if (speaker != target) and (newtarget == target):\n",
    "                        row = [u1.id, u4.id]\n",
    "                        outwriter.writerow(row)\n",
    "                        rows += 1\n",
    "print(\"done,\", rows, \"data rows written to file\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the same way, find non-adjacent pairs of same-speaker utterances:\n",
    "\n",
    "(This dataset is for testing self-coordination, which is not part of the present tutorial, but could be added.)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pairs = corpus.speaking_pairs()\n",
    "pairs_utts = corpus.pairwise_exchanges(lambda x, y: (x, y) in pairs, user_names_only=False)\n",
    "print(\"Exchange pairs:\", len(pairs_utts))\n",
    "\n",
    "outfile = open(\"movie-corpus-self-pairs.csv\", 'w')\n",
    "outwriter = csv.writer(outfile)\n",
    "\n",
    "titlerow = [\"utt_id_A\", \"utt_id_B\"]\n",
    "outwriter.writerow(titlerow)\n",
    "\n",
    "rows = 0\n",
    "for (speaker, target), utterances in pairs_utts.items():\n",
    "    for u3 in utterances:\n",
    "        if u3.reply_to in corpus.utterances:\n",
    "            u2 = corpus.utterances[u3.reply_to]\n",
    "            if u2.reply_to in corpus.utterances:\n",
    "                u1 = corpus.utterances[u2.reply_to]\n",
    "                newspeaker = u1.user\n",
    "                if (speaker == newspeaker):\n",
    "                    row = [u1.id, u3.id]\n",
    "                    outwriter.writerow(row)\n",
    "                    rows += 1\n",
    "print(\"done,\", rows, \"data rows written to file\")"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
