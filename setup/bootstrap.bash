#! /bin/bash

cd /home/jovyan/

rm -rf work

pip install tqdm

export GIT_COMMITTER_NAME=anonymous
export GIT_COMMITTER_EMAIL=anon@localhost

git clone https://yvesscherrer@bitbucket.org/yvesscherrer/lidia2019-language-change.git
