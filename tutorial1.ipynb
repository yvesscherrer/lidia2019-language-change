{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "Tutorial 1: Data analysis and visualisation\n",
    "==========\n",
    "\n",
    "- Yves Scherrer, September 2019 (minor parts by Jacob Eisenstein, May 2019)\n",
    "\n",
    "One of the most influential recent works on the linguistic and cultural analysis of historical data is the Science 2011 paper by Michel et al., [Quantitative Analysis of Culture Using Millions of Digitized Books](https://dash.harvard.edu/bitstream/handle/1/8899722/MichelScience2011.pdf?sequence=1&isAllowed=y). Their analysis relies on a dataset extracted from the Google Books collection, the \"Google N-Grams\". The [Google Ngram viewer](https://books.google.com/ngrams/) provides an easy-to-use interactive interface to explore this dataset. In this tutorial, we will show you how to replicate some of the visualisations with Python code and libraries."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "In this tutorial, we use a small part of the Google Ngram dataset. The roadmap is as follows:\n",
    "\n",
    "- Load, inspect and visualize the dataset using the Python data science stack (`pandas` for dataframes, `seaborn` for visualisation).\n",
    "- Look for different types of **noise** in the dataset.\n",
    "- Search for **neologisms** in the dataset."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Dataframes\n",
    "\n",
    "The dataset comes in the form of a CSV file, where each row represents a word, and each column represents a decade between 1700 and 2000. The values in the cell represent counts of the words. You may open the [CSV file](data/words_by_decade.csv) if you wish (but it's big!).\n",
    "\n",
    "To access this data in a useful way, we will load it as a **dataframe**. You can think of a dataframe as a spreadsheet (rows and columns of data) that you can control programmatically. In Python, the standard library for handling dataframes is called `pandas`. Let's use `pandas` to load the data.\n",
    "\n",
    "The box below contains some Python code. Click on the *Run* button to execute the code."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "import pandas\n",
    "\n",
    "df = pandas.read_csv('data/words_by_decade.csv', index_col=0)\n",
    "# index_col=0 means that the first column (the one containing the words) is used as an index\n",
    "# and is not properly part of the dataframe"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "If no error message shows up above, the dataframe has been successfully loaded. But let's check this..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "# the head command shows the first few lines of the data frame\n",
    "print(df.head(3))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "Each row represents the frequency distribution over time of a word. For example, the word `AAAAA` occurs once in the 1740 decade and 396 times in the 2000 decade. That's probably not the most linguistically interesting word though...\n",
    "\n",
    "There are also commands to display the size of the dataframe:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "outputs": [],
   "source": [
    "print(\"Size (rows / columns):\")\n",
    "print(df.shape)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "# Normalizing the counts\n",
    "\n",
    "The dataframe contains absolute frequencies of the words. However, there are much fewer books from the 1720s than from the 1990s, so that comparing absolute numbers would be unfair. Therefore, we want to normalize frequencies by decade.\n",
    "\n",
    "For this, we need to load a second CSV file that stores totals by decade. We load it again into a (different) dataframe."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "# we have already imported pandas, no need to do it again\n",
    "\n",
    "df_total = pandas.read_csv('data/totals_by_decade.csv', index_col=0)\n",
    "\n",
    "print(df_total.head())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "The `df_total` dataframe contains a single row, and the same number of columns as the `df` dataframe."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, we need to divide the absolute counts in the `df` dataframe by the totals in the `df_total` dataframe:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# loc['total'] selects the 'total' row\n",
    "# this specifies that the same row of df_total is used as a divisor for each row of df\n",
    "df_norm = df.div(df_total.loc['total'])\n",
    "print(df_norm.head(3))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The numbers obtained by the division are very small, with 7 or more zeros after the decimal point. If you prefer seeing numbers that are easier to read, you can also multiply the result by 1000:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df_norm = df.div(df_total.loc['total']) * 1000\n",
    "print(df_norm.head(3))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Plotting the data\n",
    "\n",
    "As a first example, let's select one word (e.g. the word `horse`) and plot its frequency distribution over time.\n",
    "\n",
    "For this, we will use the library `seaborn`. In order to make the plots appear directly in the notebook, we need to add two other commands related to the library `matplotlib`, on which `seaborn` is based.\n",
    "\n",
    "The following code block should not return any message."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "import matplotlib.pyplot\n",
    "%matplotlib inline\n",
    "import seaborn"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Let's now create a first plot."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "# create a dataframe consisting of a single row\n",
    "row = df_norm.loc['horse']\n",
    "row = row.loc['1800':]\n",
    "print(row.shape)\n",
    "\n",
    "# plot the 1-row dataframe as a line plot\n",
    "graph = seaborn.lineplot(data=row)\n",
    "\n",
    "# what does the following line do? comment it out to see the effect!\n",
    "x = matplotlib.pyplot.xticks(rotation=90)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "source": [
    "Does this frequency distribution correspond to your expectations? Try out some other words. You can modify the code in the block above."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can also change the time period to be plotted. The example below shows how to limit the plot to the `1800-1880` time span."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "row = df_norm.loc['horse']\n",
    "row = row.loc['1800':'1880']\n",
    "print(row.shape)\n",
    "graph = seaborn.lineplot(data=row)\n",
    "x = matplotlib.pyplot.xticks(rotation=90)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "The following code block shows how to plot the distributions of three words simultaneously."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "rows = df_norm.loc[['horse', 'dog', 'cat']]\n",
    "print(rows.shape)\n",
    "graph = seaborn.lineplot(data=rows.T)\n",
    "x = matplotlib.pyplot.xticks(rotation=90)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Go to the [Google Ngram viewer](https://books.google.com/ngrams/) and try to reproduce your experiments.\n",
    "\n",
    "Note that differences may appear because the tutorial uses a reduced dataset (no data before 1700, no rare words, only single words, aggregated by decade, ...)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "# The peak of funkitude\n",
    "\n",
    "Draw the frequency plot for the word `funk`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "row = df_norm.loc['funk']\n",
    "graph = seaborn.lineplot(data=row)\n",
    "x = matplotlib.pyplot.xticks(rotation=90)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "**Answer the following questions:**\n",
    "\n",
    "- Does the result correspond to your expectations? Can you find some evidence for this result in encyclopedias or etymological dictionaries?\n",
    "- Use again the Google Ngram viewer to plot that word. Do you see the same effect? You may have to extend the search period to 1700-2000.\n",
    "- At the bottom of the Google plot, you can search for examples in the Books corpus. Do the examples help you explain the observations? Formulate a hypothesis and back it up with other words that show similar behavior."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# A simple neologism detector\n",
    "\n",
    "A **neologism** is a word that enters common use at a particular time. Neologisms are often driven by changes in culture and technology, such that the appearance of a word often goes in pair with the appearance of the object or technique it denotes. Studying language change, and in particular neologisms, can thus shed insights about cultural changes.\n",
    "\n",
    "The Google Books dataset can be used to detect neologisms in a relatively simple way. We assume that a neologism can be defined by the following properties:\n",
    "\n",
    "- There is a specific date `d` in which its usage starts. Due to the granularity of our dataset, this can only be the beginning of a decade.\n",
    "- In the `d_before` decades preceding `d`, the average relative frequency of the word is lower than `f_max_before`.\n",
    "- In the `d_after` decades following (and including) `d`, the average relative frequency of the word is higher than `f_min_after`.\n",
    "\n",
    "The code box below defines example values for the parameters and the detection algorithm itself."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "# set the parameters\n",
    "d = '1950'            # look for a neologism that appeared in the 1950s\n",
    "d_before = 5          # consider the five preceding decades (1900, 1910, 1920, 1930, 1940)\n",
    "d_after = 5           # also consider the current and the four following decades (1950, 1960, 1970, 1980, 1990)\n",
    "f_max_before = 0.0000001 # require an average frequency of <= 0.0000001 in 1900-1940\n",
    "f_min_after = 0.0002     # require an average frequency of >= 0.0002 in 1950-1990\n",
    "\n",
    "# get the column number of d\n",
    "d_col = df_norm.columns.get_loc(d)\n",
    "# select columns\n",
    "df_neo = df_norm.iloc[:, d_col-d_before : d_col+d_before].copy()\n",
    "# compute the average frequencies before and after the cut-off date\n",
    "df_neo['avg_before'] = df_neo.iloc[:,:d_before].mean(axis=1)\n",
    "df_neo['avg_after'] = df_neo.iloc[:,d_before:d_before+d_after].mean(axis=1)\n",
    "# select columns whose averages satisfy the conditions\n",
    "df_neo = df_neo[(df_neo['avg_before'] <= f_max_before) & (df_neo['avg_after'] >= f_min_after)]\n",
    "# number of remaining rows = number of neologisms found\n",
    "print(df_neo.shape[0])\n",
    "# print the list of found neologisms\n",
    "print(sorted(list(df_neo.index)))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "**Answer the following questions:**\n",
    "\n",
    "- Write down your impressions about this list. Select a few terms, look up their meaning and their history. Is the selected decade plausible?\n",
    "- Run the neologism detector again for a different decade and write down your impressions. (Only modify the parameter `d`, not the others.)\n",
    "- The other parameters were chosen more or less randomly. Can you find better settings?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "# Appendix: How the dataset was created\n",
    "\n",
    "This part of the notebook explains how the simplified dataset used above was created. This is just for information and future reference. You are not supposed to run this code (you would have to download a lot of files and it would take a long time to run)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "- Download all 1-gram files and the *total_counts* file from the *English One Million* section of the [Google Ngram Download page](http://storage.googleapis.com/books/ngrams/books/datasetsv2.html)\n",
    "- Aggregate total counts by decade:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "import pandas, csv\n",
    "\n",
    "firstYear = 1700\n",
    "df = pandas.read_csv(\"googlebooks-eng-1M-totalcounts-20090715.txt\", sep='\\t', quoting=csv.QUOTE_NONE, header=None, skiprows=1, names=(\"year\", \"freq1\", \"freq2\", \"freq3\"))\n",
    "df = df.drop(labels=[\"freq2\", \"freq3\"], axis=1)\n",
    "df = df[df.year >= firstYear]\n",
    "df['decade'] = 10 * (df['year'] // 10)\n",
    "df = df.groupby(['decade']).sum()\n",
    "df = df.drop(labels=['year'], axis=1)\n",
    "totals = df.transpose()\n",
    "totals = totals.rename(index={'freq1': 'total'})\n",
    "totals.to_csv(\"totals_by_decade.csv\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "- Aggregate 1-grams by decade, remove years prior to 1700, remove non-alphabetic strings, merge all files:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "import pandas, csv, glob\n",
    "\n",
    "firstYear = 1700\n",
    "minTotalOcc = 1000\n",
    "full_pt = None\n",
    "\n",
    "for filename in glob.glob(\"*.csv.zip\"):\n",
    "\tprint(filename)\n",
    "\tdf = pandas.read_csv(filename, sep='\\t', quoting=csv.QUOTE_NONE, encoding='latin1', header=None, names=(\"word_orig\", \"year\", \"freq1\", \"freq2\", \"freq3\"))\n",
    "\tdf = df.drop(labels=[\"freq2\", \"freq3\"], axis=1)\n",
    "\tprint(\"  initial\", df.shape)\n",
    "\t# remove all data before firstYear\n",
    "\tdf = df[df.year >= firstYear]\n",
    "\tprint(\"  after year filter\", df.shape)\n",
    "\t# re-encode words (cannot use ignore mode during reading)\n",
    "\tdf['word'] = df['word_orig'].str.encode('latin1').str.decode('utf-8', 'ignore')\n",
    "\t# remove words with non-alphabetic characters\n",
    "\tdf = df[df['word'].str.isalpha() == True]\n",
    "\tprint(\"  after alphabetic filter\", df.shape)\n",
    "\tdf['decade'] = 10 * (df['year'] // 10)\n",
    "\tdf = df.groupby(['word', 'decade']).sum()\n",
    "\tpt = pd.pivot_table(df, values='freq1', index=['word'], columns=['decade'], fill_value=0)\n",
    "\tprint(\"  pivot before filter\", pt.shape)\n",
    "\tpt['keep'] = pt.sum(axis=1) > minTotalOcc\n",
    "\tpt = pt[pt['keep'] == True]\n",
    "\tpt = pt.drop(labels=['keep'], axis=1)\n",
    "\tprint(\"  pivot after frequency filter\", pt.shape)\n",
    "\t\n",
    "\tif full_pt is None:\n",
    "\t\tfull_pt = pt\n",
    "\telse:\n",
    "\t\tfull_pt = pd.concat([full_pt, pt])\n",
    "\tprint(\"  after concat\", full_pt.shape)\n",
    "\n",
    "full_pt.to_csv(\"words_by_decade.csv\")"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
