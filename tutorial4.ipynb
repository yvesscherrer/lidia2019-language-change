{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Interrupted Time Series\n",
    "\n",
    "- by Jacob Eisenstein, May 2019\n",
    "- adapted by Yves Scherrer, September 2019\n",
    "\n",
    "Under what conditions can we say that an event *caused* a change in a time series?\n",
    "\n",
    "If we can experimentally \"assign\" the event at random to some individuals and not others, then differences in outcomes can be attributed to the event. But in many situations, this is not possible. **Interrupted time series** analysis is a method for making causal inferences from purely observational data, but all such inferences are contingent on a set of assumptions. Interrupted time series analyses relies on **multivariate regression**.\n",
    "\n",
    "Some definitions:\n",
    "\n",
    "- **Treatment**: the event that is supposed to cause the change\n",
    "- **Outcome**: the measure of the object of change\n",
    "- **Treatment group:** individuals who are affected by the event\n",
    "- **Control group:** individuals who are not affected by the event\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Data: Hate speech on Reddit\n",
    "\n",
    "The data for this notebook is drawn from [Chandrasekharan et al 2018](https://www.cc.gatech.edu/~eshwar3/uploads/3/8/0/4/38043045/cscw2018-chandrasekharan-reddit2015ban.pdf), and is based on counts of hate speech on Reddit. Specifically:\n",
    "\n",
    "- The **treatment** is the closure of several forums in which hate speech was prevalent, which took place in June 2015.\n",
    "- The **outcome** is the count of words in a hate speech lexicon, aggregated per user and per 10 day window.\n",
    "- The **treatment group** consists of users who frequently posted in the forums that were banned in June 2015.\n",
    "- The **control group** consists of users who did not post there, but did post in similar forums.\n",
    "\n",
    "One hypothesis is that this treatment might cause hate speech to decrease, because it removed \"echo chambers\" that promote it. The null hypothesis is that the treatment has no effect: people who choose to use the words in the hate speech lexicon will continue to use them in other parts of Reddit. In this notebook we will evaluate this hypothesis."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Let's start by loading the required Python modules and the dataset:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "notes"
    }
   },
   "outputs": [],
   "source": [
    "import pandas as pd\n",
    "import statsmodels.formula.api as smf\n",
    "import seaborn as sns\n",
    "import matplotlib.pyplot as plt\n",
    "%matplotlib inline\n",
    "import numpy as np"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "df = pd.read_csv('data/reddit-counts.csv')\n",
    "print(df.head())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "- `uid`: the user ID\n",
    "- `counts`: number of words found in the hate speech lexicon \n",
    "- `condition`: whether the user is in the treatment or control group\n",
    "- `time`: number of days after the treatment event (negative: before the event, positive: after the event)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Let's plot the data using Seaborn:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "sns.lineplot(data=df, x='time', y='counts', hue='condition')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "**Questions:**\n",
    "\n",
    "- How does the closure of forums at time 0 affect the treatment group?\n",
    "- How does it affect the control group?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "We will now try to quantify these observations using a statistical model.\n",
    "\n",
    "\n",
    "## Variable intercept model\n",
    "\n",
    "The variable intercept model tries to fit a segmented regression line to the data. The intercept of the line changes at the event time, but the slope remains the same (cf. slides).\n",
    "\n",
    "In the variable intercept model, we associate an additional coefficient with the post-treatment period. Formally:\n",
    "\n",
    "\\begin{equation}\n",
    "y_{t,i} \\sim f( \\exp( \\mathbf{\\beta} \\cdot \\mathbf{x}_{t,i} + \\gamma \\delta(t > \\tau)))\n",
    "\\end{equation}\n",
    "\n",
    "where\n",
    "- $y_{t,i}$ is the outcome for individual $i$ at time $t$\n",
    "- $\\mathbf{x}_{t,i}$ is a vector of covariates, and $\\mathbf{\\beta}$ is a vector of parameters\n",
    "- $\\delta(t > \\tau)$ indicates whether $t$ is in the post-treatment period\n",
    "- $\\gamma$ quantifies the causal impact of the treatment on the intercept\n",
    "- $f$ specifies a **distribution**, such as the Normal or Poisson distribution"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "We choose the following implementation details:\n",
    "\n",
    "- We use a single covariate, namely $t$. This allows the model to correct for a linear trend in the data.\n",
    "- It might be nice to include a user-level covariate, but this poses memory challenges for `statsmodels`.\n",
    "- Since the underlying data are counts, we will use Poisson regression.\n",
    "\n",
    "Let us first add a field `post_treatment` that indicates whether the data point refers to before or after the treatment:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "df['post_treatment'] = df['time'] > 0\n",
    "print(df.head())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, we copy all rows referring to the treatment group to a new dataframe `df_treat`, and those of the control group to `df_control`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "df_treat = df.loc[df['condition']=='treatment'].copy()\n",
    "df_control = df.loc[df['condition']=='control'].copy()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Treatment group\n",
    "\n",
    "Now, we create the variable intercept model for the treatment group using the following equivalences:\n",
    "- $y_{t,i}$ corresponds to the `counts` field in the dataframe\n",
    "- $x_{t,i}$ corresponds (in our simple model) to the `time` field in the dataframe\n",
    "- $\\delta(t > \\tau)$ corresponds to the newly created column `post_treatment`.\n",
    "\n",
    "The model will infer $\\beta$ and $\\gamma$ using a `poisson` distribution:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "model_treat = smf.poisson('counts ~ time + post_treatment', data=df_treat)\n",
    "results_treat = model_treat.fit()\n",
    "print(results_treat.summary2())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "These results say:\n",
    "- For each individual in the treatment group, the base rate of hate speech lexicon matches (`Intercept`) is $e^{2.57} = 13$ per 10 days. Note that all coefficients shown in the model result table are logarithms and have to be exponentiated first (hence the $\\exp$ in the formula).\n",
    "- The change of hate speech rate in the post-treatment period is of $e^{-1.64} - 1 = 0.19 - 1 = -0.81$. The hate speech rate thus decreases by 81\\% after the treatment.\n",
    "- The general trend of hate speech usage is $e^{0.0032} = 1.0032$. Overall usage of hate speech words is thus slightly increasing, by 0.32\\%."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "The statistical model predicts a hate speech rate at each time step. Let's add these predictions to the dataframe and plot them in the same graph as the observed values:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "df_treat['fit'] = np.exp(results_treat.fittedvalues)\n",
    "print(df_treat.head())"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "ax = sns.lineplot(data=df_treat, x='time', y='counts', linewidth=0.5)\n",
    "ax = sns.lineplot(data=df_treat, x='time', y='fit', ax=ax, linewidth=3)\n",
    "ax.set_title('treatment group')\n",
    "plt.legend(['observations', 'fit'])\n",
    "ax.set_ylabel('word counts');"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Question:**\n",
    "- What does the graph show? Does the result match to your expectations?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Control group\n",
    "\n",
    "Now let's create the statistical model for the control group."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "model_control = smf.poisson('counts ~ time + post_treatment', data=df_control)\n",
    "results_control = model_control.fit()\n",
    "print(results_control.summary2())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "We can see that all coefficients are smaller than for the treatment group:\n",
    "- The base rate of hate speech usage is $e^{0.78} = 2.2$ per 10 days.\n",
    "- The change of hate speech rate in the post-treatment period is of $e^{-0.34} - 1 = 0.71 - 1 = -0.29$. The hate speech rate thus decreases by 29\\% after the treatment. The intervention thus also had an effect on the control group, albeit a weaker one than on the treatment group. This is not inconceivable, since the intervention could have discouraged hate speech throughout the site, even for individuals who were not directly affected.\n",
    "- There is also a slightly increasing overall trend of hate speech usage, but again a smaller one than for the control group.\n",
    "\n",
    "Let's plot the model predictions together with the observations in the same way:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "df_control['fit'] = np.exp(results_control.fittedvalues)\n",
    "ax = sns.lineplot(data=df_control, x='time', y='counts', linewidth=0.5)\n",
    "ax = sns.lineplot(data=df_control, x='time', y='fit', ax=ax, linewidth=3)\n",
    "ax.set_title('control group')\n",
    "plt.legend(['observations', 'fit'])\n",
    "ax.set_ylabel('word counts');"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Question:**\n",
    "- What does the graph show? Does the result match to your expectations?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Combined model\n",
    "\n",
    "To differentiate between the direct effect on the treated group (of having \"echo chamber\" forums closed) from the indirect effect on the control group (of hate speech being generally discouraged), we can model both groups together, using the following formula:\n",
    "\n",
    "``` counts ~ time + post_treatment * condition```\n",
    "\n",
    "This specifies an **interaction effect** between the treatment group (`condition`) and the post-treatment period. Coefficients are estimated for all but one of the combinations of these two variables. We use the original dataframe `df` containing all users."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "model_combined = smf.poisson('counts ~ time + post_treatment * condition', data=df)\n",
    "results_combined = model_combined.fit()\n",
    "print(results_combined.summary2())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "source": [
    "The combined model shows that:\n",
    "\n",
    "- Control users produce half as many hate speech words per 10 days after the treatment: $e^{-0.6613} = 0.52$\n",
    "- Treatment users produce almost 5 times more hate speech words overall: $e^{1.5576} = 4.84$\n",
    "- Treatment users produce 78\\% fewer hate speech words after the treatment: $e^{-0.9021} / e^{-0.6613} = 0.78$\n",
    "\n",
    "Let's create a combined plot."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "df['fit'] = np.exp(results_combined.fittedvalues)\n",
    "ax = sns.lineplot(data=df, x='time', y='counts', hue='condition', linewidth=0.5)\n",
    "ax = sns.lineplot(data=df, x='time', y='fit', hue='condition', ax=ax, linewidth=3)\n",
    "plt.legend(['treatment group', 'control group'])\n",
    "ax.set_ylabel('word counts');"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Question:**\n",
    "- Do you see any benefits in this combined plot compared to the separate plots drawn above?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Variable slopes model\n",
    "\n",
    "In some cases, the treatment affects not only the intercept, but also the slope. We can model this possibility as follows:\n",
    "\n",
    "\\begin{equation}\n",
    "y_{t,i} \\sim f( \\exp (\\mathbf{\\beta} \\cdot \\mathbf{x}_{t,i} + \\gamma \\delta(t > \\tau) + \\rho t \\delta(t > \\tau)))\n",
    "\\end{equation}\n",
    "\n",
    "where $\\rho$ quantifies the effect of the treatment on the slope."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The corresponding statistical model is defined like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "formula = 'counts ~ time + post_treatment * condition + post_treatment * condition * time'\n",
    "model_slopes = smf.poisson(formula, data=df)\n",
    "results_slopes = model_slopes.fit()\n",
    "print(results_slopes.summary2())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "source": [
    "These results tell us the following:\n",
    "- There is a negative coefficient for the interaction of the `post_treatment` period and the `time`.\n",
    "- This coefficient is even larger for the treatment group.\n",
    "- Because the magnitude of this coefficient is larger than the positive `time` coefficient alone, this indicates a reversal of slope.\n",
    "\n",
    "Let us visualize this now:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "df['fit'] = np.exp(results_slopes.fittedvalues)\n",
    "ax = sns.lineplot(data=df ,x='time', y='counts', hue='condition', linewidth=0.5)\n",
    "ax = sns.lineplot(data=df, x='time', y='fit', hue='condition', ax=ax, linewidth=3)\n",
    "plt.legend(['treatment', 'control'])\n",
    "ax.set_ylabel('word counts');"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Questions:**\n",
    "- Compare the two latest plots visually. Which statistical model provides a better fit?\n",
    "- Compare the $r^2$ (`Pseudo R-squared`) and $AIC$ values of the two models. Which statistical model provides a better fit according to these scores?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "**Question:**\n",
    "\n",
    "- For an alternative robustness check, randomly select other times as the beginning of the post-treatment period. You can do this by changing the last number in the line `df['post_treatment'] = df['time'] > 0`. Re-run the analysis from the modified line onwards. The estimated treatment effect should be smaller."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
